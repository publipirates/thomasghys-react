import React from 'react'
import {Container, Row, Column} from '../components/Grid';
import Heading from "../components/Heading";
import Customer from "../components/Customers";


const Customers = props => {
  const data = props.data;
  return (
      <Container style={{background: "#ffffff", padding: "25px"}}>
        <Row>
          <Column>
            <Heading h3>Our clients</Heading>
          </Column>
        </Row>
        <Row style={{display: "flex", alignItems: "center"}}>

          {data.allWordpressAcfCustomers.edges.map(({node}) => (
              <Column sm={12} md={4} noMargin>
                <Customer
                    imageAlt={node.acf.image.title}
                    image={node.acf.image.source_url}
                    quote={node.acf.quote}
                    id={node.wordpress_id}
                    key={node.wordpress_id}
                />
              </Column>
          ))}
        </Row>
      </Container>
  )
};

export default Customers;

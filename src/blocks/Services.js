import React from 'react'
import {Container} from '../components/Grid';
import Service from "../components/Service";

class Services extends React.Component {
  render() {
    const data = this.props.data;
    return (
        <Container>
          {data.allWordpressAcfServices.edges.map(({node}) => (
              <Service
                  id={node.wordpress_id}
                  title={node.acf.title}
                  content={node.acf.information}
                  image={node.acf.image.source_url}
                  imageAlt={node.acf.image.title}
                  hiddenContent={node.acf.extra_information}
              />
          ))}
        </Container>
    );
  }
}

export default Services;

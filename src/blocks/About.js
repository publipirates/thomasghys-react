import React from 'react'
import {Container, Row, Column} from "../components/Grid/";
import Text from "../components/Text";
import Heading from "../components/Heading";

class About extends React.Component {
  loop3(skills) {
    return (skills.information.map((information) => (
        <Text>{information.paragraph}</Text>
    )))
  };

  loop2({node}) {
    return (node.acf.skills.map((skills) => {
      const color = skills.color;
          return (
              <Column md={4}>
                {
                  React.createElement(Heading, {h5:true, primary:true, overline:true, color, children: skills.title })
                }

                {this.loop3(skills)}
              </Column>
          );
        })
    )
  };


  render() {
    const data = this.props.data;
    return (
        <Container fluid style={{
          paddingTop: "150px",
          background: "#ffffff",
          minHeight: "calc(100vh - 270px)"
        }}>
          <Container>
            <Row>
              <Column md={3}>
                <img src={data.allWordpressPage.edges[0].node.featured_media.source_url}
                     alt={data.allWordpressPage.edges[0].node.featured_media.alt_source}
                     style={{maxWidth: "100%"}}/>
                <Text textCenter>{data.allWordpressPage.edges[0].node.featured_media.title}</Text>
              </Column>
              <Column md={9}>
                <Container>
                  <Row>
                    <Column md={12}>
                      <Text>
                        {data.allWordpressPage.edges[0].node.acf.intro}
                      </Text>
                    </Column>
                  </Row>

                  <Row>
                    {data.allWordpressPage.edges.map((node) => (
                        this.loop2(node)
                    ))}
                  </Row>

                </Container>
              </Column>
            </Row>
          </Container>
        </Container>
    );
  }
}

export default About;









import React from 'react'
import HeaderComponent from "../components/Header/";
import Heading from "../components/Heading";

class Header extends React.Component {
  render() {
    const data = this.props.data;
    return (
        <HeaderComponent>
          <img src={data.wordpressAcfOptions.options.header_logo.source_url}
               alt={data.wordpressAcfOptions.options.header_logo.alt_text}
               style={{maxWidth: "100%"}}
          />
          <Heading h4 color={"white"}>
            {data.wordpressAcfOptions.options.tagline}
          </Heading>
        </HeaderComponent>
    );
  }
}

export default Header;

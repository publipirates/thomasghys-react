import React from 'react'
import Text from "../components/Text";
import Heading from "../components/Heading";
import FormStyles from "../components/Form";
import { Container, Row, Column } from '../components/Grid';



import { Form, Input, Textarea } from 'formsy-react-components';
import Button from "../components/Button";



export default class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.disableButton = this.disableButton.bind(this);
    this.enableButton = this.enableButton.bind(this);
    this.state = { canSubmit: false };
  }

  disableButton() {
    this.setState({ canSubmit: false });
  }

  enableButton() {
    this.setState({ canSubmit: true });
  }

  submit(model) {
    fetch('http://example.com/', {
      method: 'post',
      body: JSON.stringify(model)
    });
  }


  render() {
    const data = this.props.data;
    return (
        <div style={{ paddingTop: "150px" }}>
            <Container style={{ background: "#ffffff", padding: "35px" }}>
                <Row>
                  <Column>
                      <Text>{data.allWordpressPage.edges[0].node.acf.title}</Text>
                  </Column>
                  <Column>
                    <Heading h2> {data.allWordpressPage.edges[0].node.acf.subtitle}</Heading>
                  </Column>
                </Row>
                <FormStyles>
                    <Form

                        onSubmit={(data) => { console.log(data) }}
                        onValid={this.enableButton}
                        onInvalid={this.disableButton}
                        layout="horizontal">
                        <Row>
                            <Column md={6}>
                             <Input
                                 name="name"
                                 label="Name"
                                 required
                             />
                           </Column>
                          <Column md={6}>
                             <Input
                                name="email"
                                label="Email"
                                validations="isEmail"
                                validationError="This is not a valid email"
                                required
                             />
                           </Column>
                        </Row>
                        <Row>
                            <Column md={6}>
                                <Input
                                    name="company"
                                    label="Company name"
                                    required
                                />
                            </Column>
                            <Column md={6}>
                                <Input
                                    name="website"
                                    label="Website"
                                    required
                                />
                            </Column>
                        </Row>
                        <Row>
                            <Column md={12}>
                                <Textarea
                                    name="text"
                                    label="How can we help you?"
                                    required
                                />
                            </Column>
                        </Row>
                        <Row>
                            <Column>
                              <Button
                                  primary
                                  type="submit"
                                  disabled={!this.state.canSubmit}
                              >
                                Submit
                              </Button>
                            </Column>
                        </Row>
                    </Form>
                </FormStyles>
            </Container>
        </div>
    );
  }
}

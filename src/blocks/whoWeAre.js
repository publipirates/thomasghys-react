import React from 'react'
import {Container, Row, Column} from '../components/Grid';
import Heading from "../components/Heading";
import Text from "../components/Text";

class WhoWeAre extends React.Component {
  render(){
    const data = this.props.data;
    return (
        <Container style={{paddingTop: "50px", paddingBottom: "50px"}}>
          <Row style={{alignItems: "center"}}>
            <Column md={6}>
              <img src={data.allWordpressPage.edges[0].node.featured_media.source_url}
                   alt="Who we are"
                   style={{
                     display: "block",
                     maxWidth: "80%",
                     margin: "0 auto"
                   }}/>
            </Column>
            <Column md={6} style={{paddingTop: "25px"}}>
              <Heading h4 underline color={"white"}>{data.allWordpressPage.edges[0].node.acf.title}</Heading>
              <Text white>
                {data.allWordpressPage.edges[0].node.acf.information}
              </Text>
            </Column>
          </Row>
        </Container>
    );
  }
}

export default WhoWeAre;

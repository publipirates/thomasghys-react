import React from 'react'
import { graphql } from 'gatsby';
import Services from "../blocks/Services";
import Header from "../blocks/Header";
import WhoWeAre from "../blocks/whoWeAre";
import Navigation from "../components/Navigation";
import Page from "../components/Page";
import Footer from "../components/Footer";
import Customers from "../blocks/Customers";

const Index = ({data}) => {
  return (
      <Page primary>
        <Navigation data={data} style={{background: "#ffffff"}}/>
        <Header data={data}/>
        <Services data={data} style={{color: "#ffffff"}}/>
        <WhoWeAre data={data}/>
        <Customers data={data}/>
        <Footer data={data}/>
      </Page>
  )
}

export default Index;


export const query = graphql`
  query HomePageQuery {
    allWordpressPage(filter: {wordpress_id: {eq: 2}}) {
      edges {
        node {
          id
          wordpress_id
          title
          content
          featured_media {
            id
            source_url
            alt_text
          }
          acf {
            title
            information
          }
        }
      }
    }
    wordpressAcfOptions{
      id,
      wordpress_id
      options {
        header_logo {
          title,
          alt_text,
          wordpress_id,
          source_url
        },
        footer_logo {
          title,
          alt_text,
          wordpress_id,
          source_url
        },
        tagline
        addres_1
        addres_2
        addres_3
        vat
        iban
      },
    }
    allWordpressAcfServices{
      edges{
        node{
          id,
          wordpress_id,
          acf{
            title
            information
            extra_information
            image{
              source_url,
              title
            }
          }
        }
      }
    }
    allWordpressAcfCustomers{
      edges{
        node{
          wordpress_id,
          acf{
            quote
            image{
              source_url
              title
            }
          }
        }
      }
    }
  }
`

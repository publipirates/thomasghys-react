import React from 'react'
import Navigation from "../components/Navigation";
import Header from "../blocks/Header";
import Footer from "../components/Footer";
import Page from "../components/Page";
import { Container, Row, Column } from "../components/Grid/";
import Heading from "../components/Heading";
import Text from "../components/Text";
import Service from "../components/Service";


const SecondPage = () => (
    <Page primary>
      <Navigation style={{ background: "#ffffff" }}/>
      <Header />
      <Container>
        <Row style={{ marginTop: "75px" }}>
          <Column md={12}>
            <Heading h1 white>Typography</Heading>
            <Heading h2 white>Heading</Heading>
          </Column>
        </Row>
        <Row>
          <Column md={4}>
            <Heading h1>Heading 1</Heading>
            <Heading h2>Heading 2</Heading>
            <Heading h3>Heading 3</Heading>
            <Heading h4>Heading 4</Heading>
            <Heading h4>Heading 5</Heading>
            <Heading h6>Heading 6</Heading>
          </Column>
            <Column md={4}>
              <Heading h3 underline>Heading underlined</Heading>
              <Heading h3 white>Heading in white</Heading>
              <Heading h3 nomargin>Heading without margin</Heading>
            </Column>
          <Column md={4}>
            <Heading h3 >Heading default aligned</Heading>
            <Heading h3 textCenter>Heading center aligned</Heading>
            <Heading h3 textRight>Heading right aligned</Heading>
          </Column>
        </Row>
      </Container>
      <Container>
      <Row>
          <Column md={12} style={{ marginTop: "75px" }}>
            <Heading h2 white>Text</Heading>
          </Column>
        </Row>
        <Row style={{ marginTop: "25px" }}>
          <Column md={4}>
            <Text textJustified>Text justified. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
          </Column>
          <Column md={4}>
            <Text textCenter>Text Center aligned. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
          </Column>
          <Column md={4}>
            <Text textRight>Text Right aligned. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
          </Column>
        </Row>
        <Row style={{ marginTop: "25px" }}>
          <Column md={4}>
            <Text>Default text item</Text>
          </Column>
          <Column md={4}>
            <Text white>Default white text item</Text>
          </Column>
          <Column md={4}>
            <Text bold>Default text item</Text>
          </Column>
        </Row>
      </Container>
      <Container>
        <Row>
          <Column md={12} style={{ marginTop: "75px" }}>
            <Heading h2 white>Service</Heading>
          </Column>
        </Row>
        <Row>
          <Column md={12}>
            <Service
                key={1}
                title="Privacy management Roadmap"
                content={[
                  "The GDPR, ePrivacy and national legislations are challenging to navigate.",
                  <br />,
                  "We scan the compliance requirements of your organisation and design your action plan. The format of delivery ranges from a one-day workshop to an implementation track led by us."
                ]}
                image="https://picatic.global.ssl.fastly.net/events/123995/9950871b-03f1-4555-d72e-11e4f78cfbf9"
                hiddenContent={[
                  <h4>Deliverables include:</h4>,
                  <ul>
                    <li>Concrete overview of controller and processor responsibilities (GDPR art. 24-28) and template processing agreements</li>
                    <li>Pragmatic procedure for handling data subject rights from a minimal viable setup to an automated self-service model (GDPR art. 12-23)</li>
                    <li>Tactical guidelines and initial setup for data inventory (GDPR art. 30)</li>
                    <li>Outline of DPO responsibilities and governance (GDPR art. 37-39) </li>
                    <li>Dealing with international transfers of personal data (GDPR art. 44-50) with specific guidelines for cloud computing and SaaS partners</li>
                  </ul>
                ]}
            />
          </Column>
        </Row>
      </Container>
      <Footer />
    </Page>
)

export default SecondPage

import React from 'react'
import {graphql} from "gatsby";
import Page from "../components/Page";
import Navigation from "../components/Navigation";
import About from "../blocks/About";
import Footer from "../components/Footer";


const AboutPage = ({data}) => {
  return (
      <Page primary>
        <Navigation style={{background: "#ffffff"}}/>
        <About data={data}/>
        <Footer data={data}/>
      </Page>
  );
}

export default AboutPage



export const query = graphql`
  query AboutPageQuery {
    allWordpressPage(filter: {wordpress_id: {eq: 42}}) {
      edges {
        node {
          id
          wordpress_id
          title
          content
          featured_media {
            id
            source_url
            alt_text
            title
          }
          acf {
            intro,
            skills {
              title
              color
              information {
                paragraph
              }
            }
          }
        }
      }
    }
    wordpressAcfOptions{
      id,
      wordpress_id
      options {
        header_logo {
          title,
          alt_text,
          wordpress_id,
          source_url
        },
        footer_logo {
          title,
          alt_text,
          wordpress_id,
          source_url
        },
        tagline
        addres_1
        addres_2
        addres_3
        vat
        iban
      },
    }
  }
`

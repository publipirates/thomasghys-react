import React from 'react';
import {graphql} from "gatsby";
import Page from "../components/Page";
import Navigation from "../components/Navigation";
import ContactForm from "../blocks/ContactForm";
import Footer from "../components/Footer";


const ContactPage = ({data}) => {
  return (
      <Page primary>
        <Navigation data={data}/>
        <ContactForm data={data}/>
        <Footer data={data}/>
      </Page>
  )
}

export default ContactPage




export const query = graphql`
  query ContactPageQuery {
    allWordpressPage(filter: {wordpress_id: {eq: 45}}) {
      edges {
        node {
          id
          wordpress_id
          title
          acf {
            title
            subtitle
          }
        }
      }
    }
    wordpressAcfOptions{
      id,
      wordpress_id
      options {
        header_logo {
          title,
          alt_text,
          wordpress_id,
          source_url
        },
        footer_logo {
          title,
          alt_text,
          wordpress_id,
          source_url
        },
        tagline
        addres_1
        addres_2
        addres_3
        vat
        iban
      },
    }
    allWordpressAcfServices{
      edges{
        node{
          id,
          wordpress_id,
          acf{
            information
            extra_information
          }
        }
      }
    }
    allWordpressAcfCustomers{
      edges{
        node{
          wordpress_id,
          acf{
            quote
          }
        }
      }
    }
  }
`

import { darken } from 'polished';

export default {
  black: '#000',
  grayDarker: '#222',
  grayDark: '#333',
  gray: '#555',
  grayLight: '#999',
  grayLighter: '#eee',
  grayLightest: '#f1f1f1',
  grayButton: '#b7b7b7',
  white: '#fff',
  hasError: '#f00',
  success: '#17b527',
  gold: '#E2CEBB',

  primary:    '#052A3A',
  secondary:  '#793649',
  tertiary:   '#DCF2F3',
  darken: darken(0.15, '#052A3A'),
};

const breakPoints = {
  sm: '576px',
  md: '768px',
  lg: '992px',
  xl: '1200px',
};

const layout = {
  gutter: 15,
  numberOfCols: 12,
  breakPoints,
};

export default layout;

import colors from './../01.colors';
import fonts from './../02.fonts';

export default {
    textLink: {
      textColor: colors.black,
      brandedTextColor: colors.secondary,
      fontFamily: fonts.textFamily,
      headingFontFamily: fonts.headingFamily,
    },
};

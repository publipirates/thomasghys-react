import fonts from './../02.fonts';

const headingSizes = {
  h1: '30px',
  h2: '24px',
  h3: '20px',
  h4: '18px',
  h5: '16px',
  h6: '14px',
};

export default {
    heading: {
      fontFamily: fonts.gva.headingFamily,
      headingSizes,
    },
};

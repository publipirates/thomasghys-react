import colors from './../01.colors';
import fonts from './../02.fonts';

export default {
    text: {
      fontFamily: fonts.textFamily,
      textColor: colors.black,
      textColorError: colors.hasError,
      textSizes: fonts.fontSizes,
      defaultTextSize: fonts.fontSizes,
  },
};
import { darken } from 'polished';
import colors from './../01.colors';
import fonts from './../02.fonts';

export default {
  button: {
      defaultColor: colors.primary,
      defaultBorderColor: colors.primary,
      hoverDark: colors.hoverDark,
      hoverLight: colors.hoverLight,
      secondaryColor: colors.gray,
      secondaryBorderColor: colors.gray,
      defaultFont: fonts.headingFamily,
      ctaFont: fonts.textFamily,
      defaultFontSize: '18px',
      secondaryFontSize: '14px',
      textColor: colors.white,
      secondaryTextColor: colors.white,
      styledAsLinkDefaultColor: colors.primary,
      styledAsLinkNoBrandingColor: colors.black,
      linkFontFamily: fonts.textFamily,
  },
};

const textSize = '16px';

const fontSizes = {
  s: '13px',
  xs: '12px',
  xxs: '11px',
  xxxs: '8px',
};
const headingSizes = {
  h1: '30px',
  h2: '24px',
  h3: '20px',
  h4: '18px',
  h5: '16px',
  h6: '14px',
};

export default {
  textSize,
  fontSizes,
  headingSizes,
  btnColor: '#fff',
  headingFamily: 'Quicksand, "Arial", Helvetica, sans-serif',
  bodyFamily: 'Quicksand, Arial, Helvetica, sans-serif',
  textFamily: 'Quicksand, Arial, Helvetica, sans-serif',
};
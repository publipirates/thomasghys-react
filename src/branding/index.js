import { injectGlobal } from 'styled-components';
import colors from './01.colors';
import font from './02.fonts';
import layout from './03.layout';

injectGlobal`*{
    padding: 0; 
    margin: 0; 
    box-sizing: border-box; 
    text-align: left;
}`;


const brand = {
  colors,
  font,
  layout,
};

export default brand;

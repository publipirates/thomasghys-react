import styled from 'styled-components';

 const FormStyles = styled.div`
   
   form{
      padding-top: 50px;
      padding-bottom: 100px;
   }
    
   .form-group {
      margin-bottom: 20px;
   }
   
   
   label {
      text-transform: uppercase;
      font-weight: 400;
      letter-spacing: .1em;
      font-size: .7em;
   }
    
   input, textarea {
      display: block;
      width: 100%;
      padding: 1.2rem 0 .9rem;
      letter-spacing: .15em;
      color: rgba(0,0,0,.8);
      background: #fff;
      border: 0;
      border-bottom: 1px solid #d7d7d7;
      border-radius: 0;
      box-shadow: none;
      outline: 0;
      transition: all .25s ease-in;
      border: 1px solid #a7a6a6;
      cursor: text;
      font-size: .8em;
      padding: 1.25em;
      resize: none;
   }
   
   textarea{
      height: 200px;
   }
   
   
   
   .validation-message{
      position: absolute;
      display: block;
      color: #f00;
      font-size: 12px;
      font-family: Quicksand,"Arial",Helvetica,sans-serif;
      margin: 0;
    }

`

export default FormStyles;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withFormsy, propTypes as formsyProps } from 'formsy-react';
import Text from '../Text';
import { Row, Column } from './../Grid';
import { Label, Error } from './../Text/TextComponent';

const FormFieldProps = {
  ...formsyProps,
  showOptional: PropTypes.bool,
};

class FormField extends Component {
  static propTypes = FormFieldProps;

  constructor(props) {
    super(props);
    this.changeValue = this.changeValue.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.state = {
      touched: false,
    };
  }

  static defaultProps = {
    withMargin: true,
    showOptional: true,
  };

  changeValue(value) {
    this.props.setValue(value);
    this.props.onChange && this.props.onChange(value);
  }

  onBlur() {
    if (!this.state.touched) {
      this.setState({ touched: true });
    }
  }

  render() {
    const errorMsg = this.props.getErrorMessage();
    const showErr = this.state.touched;
    return (
        <div>
          <Row noMargin={!this.props.withMargin}>
            {this.props.label && (
                <Column>
                  <Label config={this.props.brand.components.text}>
                    {this.props.label}
                    {!this.props.required &&
                    !this.props.disabled &&
                    this.props.showOptional && (
                        <Text
                            s={true}
                            color={this.props.brand.colors.grayLight}
                            smallPadding={true}
                        >
                          optioneel
                        </Text>
                    )}
                  </Label>
                </Column>
            )}
            <Column>
              {React.createElement(this.props.component, {
                ...this.props,
                value: this.props.getValue() || '',
                onChange: this.changeValue,
                onBlur: this.onBlur,
              })}
            </Column>
            {showErr &&
            errorMsg && (
                <Column>
                  <Error
                      fontSize={this.props.brand.components.text.textSizes.s}
                      config={this.props.brand.components.text}
                  >
                    {errorMsg}
                  </Error>
                </Column>
            )}
          </Row>
        </div>
    );
  }
}

export default withFormsy(FormField);

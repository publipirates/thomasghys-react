import styled from 'styled-components';


const NavigationComponent = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 15px;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 500;
`;


export default NavigationComponent
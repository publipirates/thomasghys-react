import React from 'react'
import { push } from "gatsby-link";

import { Column } from '../Grid';
import NavigationComponent from "../Navigation/NavigationComponent";
import NavigationLink from "../NavigationLink";

const Navigation = () => (
    <NavigationComponent style={{ backgroundColor: "#ffffff" }}>
        <Column md={6} style={{paddingLeft: '0'}}>
          <NavigationLink noUnderline onClick={ () => push('/')}>
            Datawise
          </NavigationLink>
        </Column>
        <Column md={6} style={{display: 'flex'  , justifyContent: 'flex-end', paddingRight: '0'}}>
          <NavigationLink primary onClick={ () => push('/about')}>
            About
          </NavigationLink>
          <NavigationLink primary onClick={ () => push('/contact')}>
            Contact
          </NavigationLink>
        </Column>
    </NavigationComponent>
);

export default Navigation;
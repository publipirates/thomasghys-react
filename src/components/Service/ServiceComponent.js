import styled from 'styled-components';


const ServiceInner = styled.div`
  background-color: white;
  padding: 35px;
  margin-bottom: 25px;
  
  display: flex;
  &:nth-child(even){
    .serviceRow{
      flex-direction: row-reverse;
    }
  }

  .flipper{
    min-width: ${props => props.width}px;
    height: ${props => props.height}px;
    .ServiceInfo{
      min-width: ${props => props.width}px;
      height: ${props => props.height}px;
    }
    .ServiceImage{
      min-width: ${props => props.width}px;
      height: ${props => props.height}px;
    }
    
    
    
    width: 100%;
    transition: 0.6s;
  	transform-style: preserve-3d;
	  position: relative;
    &:hover{
      //transform: rotateY(180deg);
    }
    ${props => (props.isFlipActive 
      ? `
      transform: rotateY(180deg);
      `:`
    `)}
  }
  
  
  .ServiceImage{
    width: 100%;
	  backface-visibility: hidden;
	  position: absolute;
	  top: 0;
	  left: 0;
	  z-index: 2;
	  transform: rotateY(0deg);
	  max-height: 100%;
    background-size: cover;
    overflow: hidden;
    background-repeat: no-repeat;
    object-fit: cover;
  }
  
  
  .moreInfoButton{
    display: inline-block;
    text-decoration: underline;
    padding-bottom: 15px;
  }
`;

const ServiceInfo = styled.div`
	  backface-visibility: hidden;
	  position: absolute;
	  top: 0;
	  left: 0;
	  
	  transform: rotateY(180deg);
	  .ServiceInfo{
      padding: 0 15px;
    }
    
    h4{
      margin-bottom: 10px;
    }
    ul{
    margin-bottom: 0;
    margin-right: 0;
      li{
        font-size: 14px;
        margin-bottom: 0;
      }
    }
   
`;

const ServiceImage = styled.div`
    width: 100%;
	  backface-visibility: hidden;
	  position: absolute;
	  top: 0;
	  left: 0;
	  z-index: 2;
	  transform: rotateY(0deg);
	  max-height: 100%;
    background-size: contain;
    overflow: hidden;
`;



export { ServiceInner, ServiceInfo, ServiceImage };

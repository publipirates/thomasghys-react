import React from 'react'
import PropTypes from 'prop-types';
import Heading from '../Heading'
import Text from "../Text";
import { Container, Row, Column } from '../Grid'
import { ServiceInner, ServiceInfo } from "./ServiceComponent";



class Service extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isFlipActive: false};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isFlipActive: !prevState.isFlipActive
    }));
  }

  render() {
    const ServiceProps = {
      id: PropTypes.string,
      title: PropTypes.string,
      content: PropTypes.string,
      backgroundImage: PropTypes.string,
      hiddenContent: PropTypes.string,
      imageAlt: PropTypes.string,
    };

    return (
        <ServiceInner {...this.state} {...ServiceProps} key={this.props.id}>
          <Container>
            <Row className="serviceRow" style={{marginBottom: "0" }}>
              <Column md={6}>
                <Heading h1>{this.props.title}</Heading>
                <Text paragraph>{this.props.content}</Text>
                <span onClick={this.handleClick}
                   className="moreInfoButton"
                >Get more info...</span>
              </Column>
              <Column
                  md={6}
                  className="flipper">
                <img src={this.props.image}
                     alt={this.props.imageAlt}
                    className="ServiceImage" />
                <ServiceInfo className="ServiceInfo">
                  <Text className="ServiceInfo">
                      <div dangerouslySetInnerHTML={{ __html: this.props.hiddenContent }} />
                  </Text>
                </ServiceInfo>
              </Column>
            </Row>
          </Container>
        </ServiceInner>
    )
  }
}

export default Service;





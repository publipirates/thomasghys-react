import React from 'react'
import { CustomerComponent } from "./CustomerComponent";
import Heading from "../Heading"

class Customer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {isQuoteActive: false};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({ isQuoteActive: !this.state.isQuoteActive });
  }


  render() {
    return (
        <CustomerComponent
            {...this.state}
            key={this.props.id}
            onMouseEnter={this.handleClick}
            onMouseLeave={this.handleClick} >
          <img className="image"
            src={this.props.image}
            alt={this.props.imageAlt}
          />
          <div className="quote">
            <Heading h4 quote >
              "{this.props.quote}"
            </Heading>
          </div>
        </CustomerComponent>
    )
  }
}

export default Customer;







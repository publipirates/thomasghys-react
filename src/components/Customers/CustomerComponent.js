import styled from 'styled-components';


const CustomerComponent = styled.div`
  display: inline-block;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  img{
    object-fit: contain;
    background-repeat: no-repeat;
    margin: 0;
  }
  
  div{
    position: absolute;
    top: 0;
    justify-content: center;
    align-items: center;
    display: flex;
    width: 100%;
    height: 100%;    
  }
  
  .image, .quote{
    transition: 0.5s all;
  }
  
  .image{
    opacity: 1;
    max-width: 75%;
    display: block;
    max-height: 100px;
  }
  
  .quote{
    opacity: 0;
  }
    
    
  
  ${props => (props.isQuoteActive && `
    .image{
      opacity: 0;
    }
    .quote{
      opacity: 1;
    }    
  `)}
`;


export { CustomerComponent };

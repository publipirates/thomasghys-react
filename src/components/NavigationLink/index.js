import React from 'react';
import PropTypes from 'prop-types';
import NavigationLinkComponent from './NavigationLinkComponent';


const ButtonProps = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  noUnderline: PropTypes.bool,
};


const NavigationLink = props => {
  const handleOnClick = e => {
    props.onClick && props.onClick(e);
  };
  return (
      <NavigationLinkComponent
          onClick={e => handleOnClick(e)}
          primary={props.primary}
          secondary={props.secondary}
          cta={props.cta}
          fontSmall={props.fontSmall}
          noUnderline={props.noUnderline}
      >
        {props.children}
      </NavigationLinkComponent>
  );
};

NavigationLink.displayName = 'NavigationLink';
NavigationLink.propTypes = ButtonProps;


export default NavigationLink;

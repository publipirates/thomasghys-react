import styled from 'styled-components';
import brand from '../../branding';

export const NavigationLinkComponent = styled.a.attrs({
  type: props => props.type,
})`
  cursor: pointer;
  display: inline-block;
  border: none;
  font-family: ${props => brand.font.bodyFamily};
  font-weight: bold;
  padding: 10px 35px 3px 10px;
  outline: 0;
  
  &:last-child{
    padding-right: 10px;
  }
  &:hover{
    text-decoration: none;
  }
  
  ${props => !props.noUnderline &&`
    :after {
      content: "";
      display: block;
      width: 0%;
      height: 3px;
      transition: 0.5s all;
    }
    &:hover:after {
      width: 100%;
      height: 3px;
      transition: 0.5s width all;
    }
  `};
  
  ${props => !props.primary && !props.secondary &&`
      background:   transparent;
      color:        ${brand.colors.black};
      &:hover:after {
        background: ${brand.colors.black};
      }
  `};
  
  ${props => props.primary &&`
      background:   transparent;
      color:        ${brand.colors.primary};
      &:hover:after {
        background: ${brand.colors.primary};
      }
  `};
  
  ${props => props.secondary &&`
      background:   transparent;
      color:        ${brand.colors.secondary};
      &:hover:after {
        background: ${brand.colors.secondary};
      }
  `};
  
  ${props => props.disabled &&
    'pointer-events: none; opacity: .65;'
  } 
`;

export default NavigationLinkComponent
import styled from 'styled-components';
import brand from '../../branding';



const BaseHeading = styled.span`
  display: block;
  line-height: 150%;
  font-family: ${props => brand.font.headingFamily};
  text-align: ${props => props.align};
  margin: ${props => (props.noMargin ? '0' : '0 0 20px 0')};
  color: ${brand.colors.black};
  
  ${props => (props.overline && `
    border-top: 2px solid black;
    padding-top: 10px;
  `)}
 
  ${props => (props.underline && `
    color: black;
    border-bottom: 4px solid black;
    padding-bottom: 5px;
    display: inline-block;
    margin-bottom: 20px; 
  `)}
    
  ${props => (props.color === "white" && `
     color: ${brand.colors.white};
    border-color: ${brand.colors.white};
  `)}
  
  ${props => (props.color === "gold" && `
   color: ${brand.colors.gold};
    border-color: ${brand.colors.gold};
  `)}
  
  ${props => (props.color === "gray" && `
    color: ${brand.colors.gray};
    border-color: ${brand.colors.gray};
  `)}
    
    
    
`;



export const H1 = BaseHeading.withComponent('h1').extend`
    font-size: ${props => brand.font.headingSizes.h1 };
    line-height: 32px;
`;

export const H2 = BaseHeading.withComponent('h2').extend`
    font-size: ${props => brand.font.headingSizes.h2};
    line-height: 26px;
`;

export const H3 = BaseHeading.withComponent('h3').extend`
    font-size: ${props => brand.font.headingSizes.h3};
`;

export const H4 = BaseHeading.withComponent('h4').extend`
    font-size: ${props => brand.font.headingSizes.h4};
`;

export const H5 = BaseHeading.withComponent('h5').extend`
    font-size: ${props => brand.font.headingSizes.h5};
`;

export const H6 = BaseHeading.withComponent('h6').extend`
    font-size: ${props => brand.font.headingSizes.h6};
`;

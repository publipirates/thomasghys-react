import React from 'react';
import PropTypes from 'prop-types';
import { H1, H2, H3, H4, H5, H6 } from './HeadingComponent';

const HeaderProps = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  color: PropTypes.string,
  size: PropTypes.string,
  h1: PropTypes.bool,
  h2: PropTypes.bool,
  h3: PropTypes.bool,
  h4: PropTypes.bool,
  h5: PropTypes.bool,
  noMargin: PropTypes.bool,
  textCenter: PropTypes.bool,
  textRight: PropTypes.bool,
  underline: PropTypes.bool,
  overline: PropTypes.bool,
  white: PropTypes.bool,
  gold: PropTypes.bool,
  gray: PropTypes.bool,

};

const Heading = props => {
  const alignText = () => {
    if (props.textRight) {
      return 'right';
    } else if (props.textCenter) {
      return 'center';
    }
    return 'left';
  };

  const headingProps = {
    align: alignText(),
    color: props.color,
    noMargin: props.noMargin,
    underline: props.underline,
    white: props.white,
    gold: props.gold,
    gray: props.gray,
    overline: props.overline,
  };

  if (props.h1) {
    return <H1 {...headingProps}>{props.children}</H1>;
  } else if (props.h2) {
    return <H2 {...headingProps}>{props.children}</H2>;
  } else if (props.h3) {
    return <H3 {...headingProps}>{props.children}</H3>;
  } else if (props.h4) {
    return <H4 {...headingProps}>{props.children}</H4>;
  } else if (props.h5) {
    return <H5 {...headingProps}>{props.children}</H5>;
  }
  return <H6 {...headingProps}>{props.children}</H6>;
};

Heading.propTypes = HeaderProps;
export default Heading;

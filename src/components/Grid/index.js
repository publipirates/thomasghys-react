import PropTypes from 'prop-types';
import styled from 'styled-components';
import brand from '../../branding';


const Container = styled.div`
  margin: 0 auto;
  padding-left: ${brand.layout.gutter}px;
  padding-right: ${brand.layout.gutter}px;
  width: 100%;
 

  ${props => props.fluid &&
    `
      padding-left: 0px;
      padding-right: 0px;
    `};
    
  ${props => !props.fluid &&
    `
        @media(min-width: ${brand.layout.breakPoints.sm}){
            max-width: 540px;
        }
        @media(min-width: ${brand.layout.breakPoints.md}){
            max-width: 720px;
        }
        @media(min-width: ${brand.layout.breakPoints.lg}){
            max-width: 960px;
        }
        @media(min-width: ${brand.layout.breakPoints.xl}){
            max-width: 1140px;
        }`};
`;

Container.props = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  fluid: PropTypes.bool,
};

const Row = styled.div`
  display: flex;
  margin-left: -${brand.layout.gutter}px;
  margin-right: -${props => brand.layout.gutter}px;
  flex-wrap: wrap;
  margin-bottom: ${props =>
    props.noMargin ? '0' : `${brand.layout.gutter}px`};
`;

Row.props = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  noMargin: PropTypes.bool,
};

const Column = styled.div`
    position: relative;
    width: 100%;
    min-height: 1px;
    ${props => props.textAlign && `text-align: ${props.textAlign};`}
    ${props =>
    !props.noLeftGutter &&
    `padding-left: ${
        props.halfGutterLeft
            ? brand.layout.gutter / 2
            : brand.layout.gutter
        }px;`}
    ${props =>
    !props.noRightGutter &&
    `padding-right: ${
        props.halfGutterRight
            ? brand.layout.gutter / 2
            : brand.layout.gutter
        }px;`}
    ${props =>
    props.sm &&
    `
        flex: 0 0 ${100 / (brand.layout.numberOfCols / props.sm)}%; 
        max-width: ${100 / (brand.layout.numberOfCols / props.sm)}%;
    `}

    ${props =>
    props.valign &&
    `
      margin-top: auto;
      margin-bottom: auto;
    `}
    ${props =>
    props.md &&
    `
        @media(min-width: ${brand.layout.breakPoints.md}){
            flex: 0 0 ${100 / (brand.layout.numberOfCols / props.md)}%;
            max-width: ${100 / (brand.layout.numberOfCols / props.md)}%; 
        }
    `}
    ${props =>
    props.lg &&
    `
        @media(min-width: ${brand.layout.breakPoints.lg}){
            flex: 0 0 ${100 / (brand.layout.numberOfCols / props.lg)}%; 
            max-width: ${100 / (brand.layout.numberOfCols / props.lg)}%;
        }
    `}
    ${props =>
    props.xl &&
    `
        @media(min-width: ${brand.layout.breakPoints.xl}){
            flex: 0 0 ${100 / (brand.layout.numberOfCols / props.xl)}%; 
            max-width: ${100 / (brand.layout.numberOfCols / props.xl)}%;
        }
    `}

    ${props =>
    props.offsetSm &&
    `
           margin-left: ${100 /
    (brand.layout.numberOfCols / props.offsetSm)}%;
    `}
    ${props =>
    props.offsetMd &&
    `
        @media(min-width: ${brand.layout.breakPoints.md}){
            margin-left: ${100 /
    (brand.layout.numberOfCols / props.offsetMd)}%;
        }
    `}
    ${props =>
    props.offsetLg &&
    `
        @media(min-width: ${brand.layout.breakPoints.lg}){
            margin-left: ${100 /
    (brand.layout.numberOfCols / props.offsetLg)}%;
        }
    `}
    ${props =>
    props.offsetXl &&
    `
        @media(min-width: ${brand.layout.breakPoints.xl}){
            margin-left: ${100 /
    (brand.layout.numberOfCols / props.offsetXl)}%;
        }
    `}
`;

Column.props = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xl: PropTypes.number,
  offsetSm: PropTypes.number,
  offsetMd: PropTypes.number,
  offsetLg: PropTypes.number,
  offsetXl: PropTypes.number,
  noLeftGutter: PropTypes.bool,
  noRightGutter: PropTypes.bool,
  textAlign: PropTypes.string,
  className: PropTypes.string,
};

export { Container, Row, Column };

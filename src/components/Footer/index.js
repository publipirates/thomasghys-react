import React from 'react'
import { Container, Row, Column } from '../Grid'
import Text from '../Text'
import Button from "../Button";
import {push} from "gatsby-link";


class Footer extends React.Component {
  render() {
    const data = this.props.data;
    return (
        <Container style={{paddingTop: "50px", paddingBottom: "50px"}}>
          <Row>
            <Column md={4}>
              <img
                  src={data.wordpressAcfOptions.options.footer_logo.source_url}
                  alt={data.wordpressAcfOptions.options.footer_logo.alt_text}
                  style={{width: "80%"}}
              />
            </Column>
            <Column md={4}>
              <Text white>
                {data.wordpressAcfOptions.options.addres_1}<br/>
                {data.wordpressAcfOptions.options.addres_2}<br/>
                {data.wordpressAcfOptions.options.addres_3}
              </Text>
              <Text gray>
                VAT: {data.wordpressAcfOptions.options.vat}<br/>
                IBAN: {data.wordpressAcfOptions.options.iban}
              </Text>
            </Column>
            <Column md={4}>
              <Button onClick={ () => push('/contact')}>
                Let's get in touch
              </Button>
            </Column>
          </Row>
        </Container>
    )
  }
};

export default Footer;

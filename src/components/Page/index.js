import brand from "../../branding";
import styled from 'styled-components';


const Page = styled.div`
    background-color: ${props => props.primary ? brand.colors.primary : brand.colors.white};
`;


export default Page;

import React from 'react';
import PropTypes from 'prop-types';
import { ButtonComponent, ButtonLink, LinkButton } from './ButtonComponent';

const ButtonProps = {
  asLink: PropTypes.bool,
  branded: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  cta: PropTypes.bool,
  disabled: PropTypes.bool,
  fontSmall: PropTypes.bool,
  href: PropTypes.string,
  onClick: PropTypes.func,
  outline: PropTypes.bool,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  secondaryLinkColor: PropTypes.bool,
  styledAsLink: PropTypes.bool,
  target: PropTypes.string,
  type: PropTypes.string,
};

const Button = props => {
  const handleOnClick = e => {
    props.onClick && props.onClick(e);
  };

  if (props.asLink) {
    return (
        <ButtonLink
            onClick={e => handleOnClick(e)}
            secondary={props.secondary}
            disabled={props.disabled}
            cta={props.cta}
            href={props.href}
            target={props.target}
            fontSmall={props.fontSmall}
            outline={props.outline}
        >
          {props.children}
        </ButtonLink>
    );
  } else if (props.styledAsLink) {
    return (
        <LinkButton
            type={props.type}
            onClick={e => handleOnClick(e)}
            secondary={props.secondary}
            disabled={props.disabled}
            cta={props.cta}
            outline={props.outline}
            fontSmall={props.fontSmall}
            secondaryLinkColor={
              props.secondaryLinkColor && props.brand.colors.secondary
            }
        >
          {props.children}
        </LinkButton>
    );
  }

  return (
      <ButtonComponent
          type={props.type}
          onClick={e => handleOnClick(e)}
          primary={props.primary}
          secondary={props.secondary}
          disabled={props.disabled}
          cta={props.cta}
          outline={props.outline}
          fontSmall={props.fontSmall}
      >
        {props.children}
      </ButtonComponent>
  );
};

Button.displayName = 'Button';
Button.propTypes = ButtonProps;
Button.defaultProps = {
  branded: true,
  secondaryLinkColor: false,
};

export default Button;

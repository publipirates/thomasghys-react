import styled from 'styled-components';
import brand from "../../branding";

export const ButtonComponent = styled.button.attrs({
  type: props => props.type,
  href: props => props.href,
  target: props => props.target,
})`
  cursor: pointer;
  display: inline-block;
  border: ${props => (props.outline ? '2px solid' : 'none')};
  padding: 17px 17px 13px 17px;
  outline: 0;
  text-align: center;
  font-weight: bold;
  
  &[type=submit]{
    float: right;
  }
  
  
  
  ${props => props.disabled && '' +
    'pointer-events: none; ' +
    'opacity: .45;'
  } 
  
  ${props => props.primary && `
    background-color: ${brand.colors.primary} 
    color: ${brand.colors.white};
    transition: all .5s;
    &:hover{
      background-color: ${brand.colors.secondary};
      transition: all .5s;
    }
   `} 
  
  font-size: ${props => props.fontSmall
    ? brand.font.secondaryFontSize
    : brand.font.defaultFontSize};
`;

export const ButtonLink = ButtonComponent.withComponent('a').extend`
    text-decoration: none;
`;

export const LinkButton = styled.button.attrs({
  type: props => props.type,
})`
  color: red;
  font-family: brand.font.fontFamily;
  text-decoration: ${props => (props.decorated ? 'underline' : 'none')};
  cursor: pointer;
  text-decoration: underline;
  background: transparent;
  border: none;
  font-size: ${ brand.font.secondaryFontSize };
`;

import styled from 'styled-components';

const HeaderComponent = styled.header`
  margin: 0 auto;
  padding-top: 125px;
  padding-bottom: 25px;
  max-width: 80%;
  width: 300px;
`;

export default HeaderComponent

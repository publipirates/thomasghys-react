import styled from 'styled-components'

const Label = styled.label`
    width: 100%;
`;


const Input = styled.input`
    width: 100%;
`;

export { Label, Input }
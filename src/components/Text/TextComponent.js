import styled from 'styled-components';
import brand from "../../branding";

export const TextComponent = styled.span`
  display:        block;
  margin-bottom:  10px;
  line-height:    150%;
  color:          #000000

  font-family:    ${props => brand.font.bodyFamily};
  font-size:      ${props => brand.font.textSize};
  text-align:     ${props => props.align};
  font-weight:    ${props => (props.bold ? 'bold' : 'normal')};
  
  ${props => (props.white && `
    color: ${brand.colors.white};
  `)}
  
  ${props => (props.gray && `
    color: ${brand.colors.grayLight};
  `)}
`;

export const Paragraph = TextComponent.withComponent('p').extend`
    margin:       0 0 ${props => (props.noMargin ? '0' : '20px')} 0;
`;

export const Label = styled.label.attrs({
  htmlFor: props => props.for,
})`
  font-family:    ${props => brand.font.fontFamily};
  font-size:      ${props => brand.font.fontSize};
  text-align:     ${props => props.align};
  color:          ${props => brand.colors.textColor};
  margin:         0 0 ${props => (props.noMargin ? '0' : '10px')} 0;
  display: block;
`;

export const Error = TextComponent.withComponent('p').extend`
    display:      block;
    margin:       10px 0;
    font-weight:  700;
    color:        ${props => brand.colors.hasError};
`;

import React from 'react';
import PropTypes from 'prop-types';
import { TextComponent, Paragraph, Error, Label } from './TextComponent';
import brand from '../../branding'

const TextProps = {
  bold: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  fontSize: PropTypes.string,
  for: PropTypes.string,
  isError: PropTypes.bool,
  label: PropTypes.bool,
  noMargin: PropTypes.bool,
  paragraph: PropTypes.bool,
  s: PropTypes.bool,
  text: PropTypes.object,
  textCenter: PropTypes.bool,
  textJustified: PropTypes.bool,
  textRight: PropTypes.bool,
  xs: PropTypes.bool,
  xxs: PropTypes.bool,
  xxxs: PropTypes.bool,
  white: PropTypes.bool,
  gray: PropTypes.bool,
  className: PropTypes.string,
};

const Text = props => {
  const getFontSize = () => {
    if (props.fontSize) {
      return props.fontSize;
    } else if (props.s) {
      return brand.textSizes.s;
    } else if (props.xs) {
      return brand.textSizes.xs;
    } else if (props.xxs) {
      return brand.textSizes.xxs;
    } else if (props.xxxs) {
      return brand.textSizes.xxxs;
    }
    return brand.defaultTextSize;
  };

  const alignment = () => {
    if (props.textRight) {
      return 'right';
    } else if (props.textCenter) {
      return 'center';
    } else if (props.textJustified) {
      return 'justify';
    }

    return 'left';
  };

  const textProps = {
    fontSize: getFontSize(),
    align: alignment(),
    bold: props.bold,
    noMargin: props.noMargin,
    white: props.white,
    gray: props.gray,
    className: props.className,
  };

  if (props.paragraph) {
    return <Paragraph {...textProps}>{props.children}</Paragraph>;
  } else if (props.label) {
    return (
        <Label {...textProps} for={props.for}>
          {props.children}
        </Label>
    );
  } else if (props.isError) {
    return <Error {...textProps}>{props.children}</Error>;
  }
  return <TextComponent {...textProps}>{props.children}</TextComponent>;
};

Text.displayName = 'Text';
Text.propTypes = TextProps;

export default Text;
